// This file is part of the Pthread C Code generator toolbox
// 
// Copyright (C) 2011 - DIGITEO - Clément DAVID <clement.david@scilab.org>
// Copyright (C) 2011-2012 - Scilab Enterprises - Clément DAVID <clement.david@scilab-enterprises.com>
// see license.txt for more licensing informations


function vec = ptccgen_utils_toBinary(v, len)
    // Return a vector filled with 0 and 1 which represent binary transform of v
    //
    // Calling Sequence
    //   vec = ptccgen_utils_toBinary(v, szclkIN)
    //
    // Parameters
    // v: double: number to represent
    // len: double: length of the output vector
    // vec: len vector of [0,1]: binary representation of v
    
    if v >= 2^32 then
        error(msprintf(gettext("%s: Wrong value for input argument #%d: Must be in the interval [%s, 2^%s -1].\n"), "ptccgen_utils_toBinary", 1, string(0), string(len)));
    end
    
    vec=zeros(1,len)
    for i=1:len
        w=v/2;
        vec(i)=v-2*int(w);
        v=int(w);
    end
endfunction

