// This file is part of the Pthread C Code generator toolbox
// 
// Copyright (C) 2011 - DIGITEO - Clément DAVID <clement.david@scilab.org>
// Copyright (C) 2011-2012 - Scilab Enterprises - Clément DAVID <clement.david@scilab-enterprises.com>
// see license.txt for more licensing informations


function my_config = ptccgen_config(v1, v2, v3)
    // Configuration constructor.
    //
    // Calling Sequence
    //     my_config = ptccgen_config()
    //     my_config = ptccgen_config(block, out, options);
    //     my_config = ptccgen_config(options="standalone");
    //
    // Parameters
    // block: scicos_block: the block instance parameters.
    // out: ptccgen_config_out: Setting for generated code.
    // options: struct: key-values vendor-specific options
    // my_config: tlist: the configuration structure

    if ~exists("scicos_block()") then loadXcosLibs(); end

    if ~exists('block', 'local') then block = scicos_block(); end
    if ~exists('out', 'local') then out = ptccgen_config_out(); end
    if ~exists('options', 'local') then options = struct("standalone", 0, "updateDiagram", 1); end
    
    my_config = tlist(["ptccgen_config", ..
        "block", "out", "options"], ..
        block, out, options);
endfunction

function out = ptccgen_config_out(v1, v2 , v3, v4, v5)
    // Output configuration constructor.
    //
    // Calling Sequence
    //     config = ptccgen_config_out()
    //     config = ptccgen_config_out(interface, out, options);
    //     config = ptccgen_config_out(options="standalone");
    //
    // Parameters
    // output: ptccgen_config_out: the block instance parameters.
    // out: ptccgen_config_out: Setting for generated code.
    // options: struct: key-values vendor-specific options
    // config: tlist: the configuration structure

    if ~exists('name', 'local') then name = ""; end
    if ~exists('workingdir', 'local') then workingdir = TMPDIR + "/" + name; end
    
    [CFLAGS, CFILES, LDFLAGS, LFILES] = getStandardFlags();
    if ~exists('cflags', 'local') then cflags = CFLAGS; end
    if ~exists('cfiles', 'local') then cfiles = CFILES; end
    if ~exists('ldflags', 'local') then ldflags = LDFLAGS; end
    if ~exists('lfiles', 'local') then lfiles = LFILES; end
    
    out = tlist(["ptccgen_config_out", ..
        "name", "workingdir", "cflags", "cfiles", "ldflags", "lfiles"], ..
        name, workingdir, cflags, cfiles, ldflags, lfiles);
endfunction

function [cflags, cfiles, ldflags, lfiles] = getStandardFlags()
    // Get the standard flags and files
    //
    // Calling Sequence
    //     [cflags, cfiles, ldflags, lfiles] = getStandardFlags()
    //
    // Parameters
    // cflags: column vector of string: default CFLAGS
    // cfiles: column vector of string: files to compile
    // ldflags: column vector of string: default LDFLAGS
    // lfiles: column vector of string: libraries to link with
    
    cflags = [];
    cfiles = [];
    ldflags = [];
    lfiles = [];

    if getos() <> "Windows" then
        if isdir(SCI+"/../../include/scilab/scicos_blocks/") then
            // Linux/Mac/Unix binary version
            cflags = [cflags ; " -I" + SCI + "/../../include/scilab/scicos/"];
            cflags = [cflags ; " -I" + SCI + "/../../include/scilab/scicos_blocks/"];
            cflags = [cflags ; " -I" + SCI + "/../../include/scilab/dynamic_link/"];
            
            lfiles = [lfiles ; SCI + "/../../lib/scilab/libsciscicos.so"];
            lfiles = [lfiles ; SCI + "/../../lib/scilab/libsciscicos_blocks.so"];
        else	  
            // Linux/Mac/Unix source version 
            cflags = [cflags ; " -I" + SCI + "/modules/scicos/includes/"];
            cflags = [cflags ; " -I" + SCI + "/modules/scicos_blocks/includes/"];
            cflags = [cflags ; " -I" + SCI + "/modules/dynamic_link/includes/"];
            
            lfiles = [lfiles ; SCI + "/modules/scicos/.libs/libsciscicos.so"];
            lfiles = [lfiles ; SCI + "/modules/scicos_blocks/.libs/libsciscicos_blocks.so"];
        end
    else
        // Windows version
        cflags = [cflags ; " -I""" + SCI + "/modules/scicos/includes/"""];
        cflags = [cflags ; " -I""" + SCI + "/modules/scicos_blocks/includes/"""];
        cflags = [cflags ; " -I""" + SCI + "/modules/dynamic_link/includes/"""];

        lfiles = [lfiles ; SCI + "/scicos.dll"];
        lfiles = [lfiles ; SCI + "/scicos_f.dll"];
        lfiles = [lfiles ; SCI + "/scicos_blocks.dll"];
        lfiles = [lfiles ; SCI + "/scicos_blocks_f.dll"];

        // ldflags for Visual studio
        if findmsvccompiler() <> "unknown" & haveacompiler() then
            ldflags = [ldflags ; " """ + SCI + "/bin/scicos.lib"""];
            ldflags = [ldflags ; " """ + SCI + "/bin/scicos_f.lib"""];
            ldflags = [ldflags ; " """ + SCI + "/bin/scicos_blocks.lib"""];
            ldflags = [ldflags ; " """ + SCI + "/bin/scicos_blocks_f.lib"""];
        end
    end
endfunction

