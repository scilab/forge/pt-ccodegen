// This file is part of the Pthread C Code generator toolbox
// 
// Copyright (C) 2011 - DIGITEO - Clément DAVID <clement.david@scilab.org>
// Copyright (C) 2011-2012 - Scilab Enterprises - Clément DAVID <clement.david@scilab-enterprises.com>
// see license.txt for more licensing informations


function identifier = ptccgen_utils_toCName(identifier)
    // Generate a valid identifier name.
    //
    // Calling Sequence
    //   identifier = ptccgen_utils_toCName(identifier)
    //
    // Parameters
    // identifier: a string: A superblock diagram to use for code generation
    //

    // translate space to under underscore
    identifier = strsubst(identifier, "/\s/", '_', 'r');
    // remove leading digitals
    identifier = strsubst(identifier, "/^\d*/", '', 'r');
    // remove non-identifier symbols
    identifier = strsubst(identifier, "/^\W*/", '', 'r');
endfunction

