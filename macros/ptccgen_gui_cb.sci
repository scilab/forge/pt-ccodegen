// This file is part of the Pthread C Code generator toolbox
// 
// Copyright (C) 2011 - DIGITEO - Clément DAVID <clement.david@scilab.org>
// Copyright (C) 2011-2012 - Scilab Enterprises - Clément DAVID <clement.david@scilab-enterprises.com>
// see license.txt for more licensing informations


function ptccgen_gui_cb(status)
    // ptccgen_gui figure global callback
    //
    // Calling Sequence
    //   ptccgen_gui_cb("ok");
    //   ptccgen_gui_cb("cancel");
    //
    // Parameters
    // status: one of "ok", "cancel"
    //
    
//-- Error helpers
// In case of error, a message is printed into the message bar and the
// execution is aborted.
    
    function clearMsgError()
        h = findobj("Tag", "ptccgen_msgText");
        set(h, "String", "");
    endfunction

    function setMsgError(msg)
        h = findobj("Tag", "ptccgen_msgText");
        set(h, "String", msg);
        abort;
    endfunction
//--
    
    ptccgenFigure = findobj("Tag", "ptccgenFigure");
    if status == "cancel" then
        close(ptccgenFigure);
        return;
    end
    
    if status == "ok" then
    
        [my_config, cpr, data] = getGuiData(ptccgenFigure);
        ALWAYS_ACTIVE = data.ALWAYS_ACTIVE;
        [act, cap, allhowclk, allhowclk2] = data.io(:);
        FIRING = data.FIRING;
        [clkIN clkOUT] = data.clk(:);
        bllst=data.bllst;
        
        // helpers variables
        x=cpr.state.x;
        z=cpr.state.z;
        outtb=cpr.state.outtb;
        
        zcptr=cpr.sim.zcptr;
        ozptr=cpr.sim.ozptr;
        rpptr=cpr.sim.rpptr;
        ipptr=cpr.sim.ipptr;
        opptr=cpr.sim.opptr;
        funs=cpr.sim.funs;
        xptr=cpr.sim.xptr;
        zptr=cpr.sim.zptr;
        inpptr=cpr.sim.inpptr;
        inplnk=cpr.sim.inplnk;
        outptr=cpr.sim.outptr;
        outlnk=cpr.sim.outlnk;
        ordclk=cpr.sim.ordclk;
        funtyp=cpr.sim.funtyp;
        cord=cpr.sim.cord;
        nblk=cpr.sim.nb;
        ztyp=cpr.sim.ztyp;
        clkptr=cpr.sim.clkptr
        
        // calculated helpers
        ncord=size(cord,1);
        nztotal=size(z,1);
        
        maxnrpar=max(rpptr(2:$)-rpptr(1:$-1))
        maxnipar=max(ipptr(2:$)-ipptr(1:$-1))
        maxnx=max(xptr(2:$)-xptr(1:$-1))
        maxnz=max(zptr(2:$)-zptr(1:$-1))
        maxnin=max(inpptr(2:$)-inpptr(1:$-1))
        maxnout=max(outptr(2:$)-outptr(1:$-1))
        maxdim=[];
        for i=1:lstsize(cpr.state.outtb)
            maxdim=max(size(cpr.state.outtb(i)))
        end
        maxtotal=max([maxnrpar;maxnipar;maxnx;maxnz;maxnin;maxnout;maxdim]);
        
        // code emitter loop on blocks
        nbcap=0;nbact=0;
        
        fds = list(); // (filename, file descriptor)s list
        state = struct("config", my_config); // global data stash
        
        // add some fields to the data stash
        state.capt = [] ; // sensor index
        state.actt = [] ; // actor index
        
        for i=1:length(funs)
            if or(i==act) then //block is an actuator
                [fds, state] = ptccgen_emit_call_actor(fds, state, cpr, i);
            elseif or(i==cap) then //block is a actuator
                [fds, state] = ptccgen_emit_call_sensor(fds, state, cpr, i);
            elseif funs(i)=='bidon' then // block is an event input
            elseif funs(i)=='bidon2' then // block is an event output
            else // any block
                [fds, state] = ptccgen_emit_call_block(fds, state, cpr, i);
            end
        end // i=1:length(funs)
        
        // code emitter for scheduler
        [fds, state] = ptccgen_emit_scheduler(fds, state, cpr, i);
        
        // code emitter for interface function
        fds = ptccgen_emit_interface(fds, my_config, cpr, data);
        
        for i=2:2:length(fds)
            msprintf("closing: %s - %d\n", fds(i-1), fds(i));
            mclose(fds(i));
        end
        
        // export and build the txt code as a block
        ptccgen_build(my_config, txt)
        XX=update_block(XX);
    end
endfunction

function [my_config, cpr, data] = getGuiData(ptccgenFigure)
    // Retrieve the configuration options from the UI and perform parameter
    // validation.
    //
    // Error reporting is done using clearMsgError() and setMsgError(msg)
    
    clearMsgError();
    UserData = get(ptccgenFigure, "UserData");
    [my_config, cpr, data] = UserData(:);
    clear UserData;
    
    // block name
    h = findobj("Tag", "ptccgen_blkNameTxt");
    str = get(h, "String");
    my_config.out.name = ptccgen_utils_toCName(str);
    if str <> my_config.out.name then
        setMsgError(msprintf(gettext("Block name error: invalid C identifier. <br>Modified to %s."), my_config.out.name));
    end
    
    // working dir
    h = findobj("Tag", "ptccgen_targetDirTxt");
    str = get(h, "String");
    [status, msg] = mkdir(str);
    if ~or(status == [1 2]) then
        setMsgError("Working dir error: " + msg);
    end
    my_config.out.workingdir = str;
    
    // cflags
    h = findobj("Tag", "ptccgen_CflagsLst");
    str = get(h, "String") + "";
    my_config.out.cflags = str;
    
    // cfiles
    h = findobj("Tag", "ptccgen_CfilesLst");
    str = get(h, "String") + "";
    my_config.out.cflags = str;
    
    // ldflags
    h = findobj("Tag", "ptccgen_LflagsLst");
    str = get(h, "String") + "";
    my_config.out.ldflags = str;
    
    // cfilesresume
    h = findobj("Tag", "ptccgen_LfilesLst");
    str = get(h, "String") + "";
    my_config.out.cflags = str;
endfunction

