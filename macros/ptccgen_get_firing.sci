// This file is part of the Pthread C Code generator toolbox
// 
// Copyright (C) 2011 - DIGITEO - Clément DAVID <clement.david@scilab.org>
// Copyright (C) 2011-2012 - Scilab Enterprises - Clément DAVID <clement.david@scilab-enterprises.com>
// see license.txt for more licensing informations


function FIRING = ptccgen_get_firing(bllst, clkconnect, allhowclk2)
    // Get the output events list.
    //
    // Calling Sequence
    //   FIRING = ptccgen_get_firing(bllst, clkconnect)
    //
    // Parameters
    // bllst: list of blocks: the pre-compiled block list
    // clkconnect: nx4: clock connection matrix
    // FIRING: vector index: index of the fired events
    
    FIRING=[]
    for i=1:size(allhowclk2,1)
        j = find(clkconnect(:,3) == allhowclk2(i))
        if j<>[] then
            FIRING=[FIRING ; bllst(clkconnect(j,1)).firing(clkconnect(j,2))]
        end
    end
endfunction
