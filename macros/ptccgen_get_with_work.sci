// This file is part of the Pthread C Code generator toolbox
// 
// Copyright (C) 2011 - DIGITEO - Clément DAVID <clement.david@scilab.org>
// Copyright (C) 2011-2012 - Scilab Enterprises - Clément DAVID <clement.david@scilab-enterprises.com>
// see license.txt for more licensing informations


function with_work = ptccgen_get_with_work(cpr)
    // Get the block mask for blocks which use the work field.
    //
    // Calling Sequence
    //   with_work = ptccgen_get_with_work(cpr)
    //
    // Parameters
    // cpr: tlist: the simulation structure
    // with_work: nx1: vector mask of blocks which use work.
    
    with_work = zeros(cpr.sim.nblk, 1) == 1;
    
    // before checking clear I/O
    funs_save=cpr.sim.funs;
    funtyp_save=cpr.sim.funtyp;
    for i=1:size(cpr.sim.funs)
        if part(cpr.sim.funs(i),1:5)=='input' then
            cpr.sim.funs(i) ='bidon'
            cpr.sim.funtyp(i) = 1
        elseif part(cpr.sim.funs(i),1:6)=='output' then
            cpr.sim.funs(i) ='bidon'
            cpr.sim.funtyp(i) = 1
        end
    end
    
    // storing opened windows
    before_wins = winsid();
    
    // execute init and stop and check the work pointer value.
    ierr=execstr('[state,t]=scicosim(cpr.state,0,0,cpr.sim,''start'',scs_m.props.tol)','errcatch');
    if ierr==0 then
        for i=1:cpr.sim.nblk
            with_work(i) = state.iz(i) <> 0;
        end
        ierr=execstr('[state,t]=scicosim(state,0,0,cpr.sim,''finish'',scs_m.props.tol)','errcatch');
    end
    
    // closing opened windows
    after_wins = winsid();
    [v, ka, kb] = union(before_wins, after_wins);
    for i=after_wins(kb)
        close(figure(i));
    end
    
    // reset I/O functions
    cpr.sim.funs = funs_save;
    cpr.sim.funtyp = funtyp_save;
endfunction

