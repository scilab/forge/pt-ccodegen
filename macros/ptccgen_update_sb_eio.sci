// This file is part of the Pthread C Code generator toolbox
// 
// Copyright (C) 2011 - DIGITEO - Clément DAVID <clement.david@scilab.org>
// Copyright (C) 2011-2012 - Scilab Enterprises - Clément DAVID <clement.david@scilab-enterprises.com>
// see license.txt for more licensing informations


function [bllst, clkconnect, corinv, howclk] = ptccgen_update_sb_eio(bllst, szclkIN,..
    ALWAYS_ACTIVE, clkconnect, corinv, howclk, howclk2, clkIN)
    // Add all the I/O blocks to the block list and update correlation 
    // matrix accordingly
    //
    // Calling Sequence
    //   [bllst, clkconnect, corinv] = ptccgen_update_sb_eio(bllst, szclkIN,..
    //                  ALWAYS_ACTIVE, clkconnect, corinv, howclk, howclk2)
    //
    // Parameters
    // bllst: block list: block lists
    // szclkIN: 2x1 matrix: size of the clock input
    // ALWAYS_ACTIVE: boolean: flag if the generated code should be always active
    // clkconnect: nx4 matrix: block activation vector
    // corinv: matrix: Correlation matrix between compiled structure and diagram
    // howclk: index: index of the global event input block
    // clkIN: vector index: Index of the event input blocks in the list

//HELPERS
    function [bllst, clkconnect, corinv] = add_fictious_multi(bllst, szclkIN, clkconnect, corinv)
        // Replace all events inputs with a single block (keep the same number of events)
        //
        // Parameters
        // bllst: n-block list: block lists
        // szclkIN: 2x1 matrix: size of the clock input
        // clkconnect: nx4 matrix: block activation data
        // corinv: matrix: Correlation matrix between compiled structure and diagram
        
        // add the block
        output=ones((2^szclkIN)-1,1)
        bllst($+1)=scicos_model(..
            sim=list('bidon',1),..
            evtout=output,..
            blocktype='d',..
            firing=-output',..
            dep_ut=[%f %f]);
        corinv(size(bllst))=size(bllst)+1;
        
        // adjust the links accordingly
        for i=1:(2^szclkIN)-1
            vec=ptccgen_utils_toBinary(i,szclkIN)
            for j=1:szclkIN
                if vec(j)*clkIN(j)>=1 then
                    for k=1:size(clkconnect,1)
                        if clkconnect(k,1)==clkIN(j) then
                            clkconnect=[clkconnect;[howclk i clkconnect(k,3:4)]]
                        end
                    end
                end
            end
        end
    endfunction
    
    function [bllst, corinv] = add_fictious_single(bllst, corinv)
        // Add an event block to the block list
        //
        // Parameters
        // bllst: block list: block lists
        // corinv: matrix: Correlation matrix between compiled structure and diagram
        
        // add the block
        output = ones((2^(size(cap,'*')))-1,1);
        if (output == []) then
            output = 0;
        end
        bllst($+1)=scicos_model(..
            sim=list('bidon',1),..
            evtout=output,..
            firing=-output,..
            blocktype='d',..
            dep_ut=[%f %f]);
        
        // link with a future block
        corinv(size(bllst)) = size(bllst) + 1;
    endfunction
    
    function clkconnect = sort_clkconnect(szclkIN, clkconnect)
        // Update event input connections after modification
        //
        // Parameters
        // clkconnect: nx4 matrix: block activation data
        
        newclkconnect=clkconnect;
        clkconnect=[];
        for i=1:size(newclkconnect,1)-1
            if or(newclkconnect(i,:)<>newclkconnect(i+1,:)) then
                clkconnect=[clkconnect;newclkconnect(i,:)]
            end
        end
        if or(newclkconnect($-1,:)<>newclkconnect($,:)) then
            clkconnect=[clkconnect;newclkconnect($,:)]
        end

        // delete event input ports
        newclkconnect=clkconnect;nkt=[];
        for i=1:szclkIN
            for k=1:size(newclkconnect,1)
                if newclkconnect(k,1)~=clkIN(i) then
                    nkt=[nkt;newclkconnect(k,:)];
                end
            end
            newclkconnect=nkt;
            nkt=[];
        end
        clkconnect=newclkconnect;
    endfunction
//ENDHELPERS

    // getting howclk, the event input block index
    if szclkIN > 1 then
        // When multiple events inputs, add a event mux block with the same number of events.
        [bllst, clkconnect, corinv] = add_fictious_multi(bllst, szclkIN, clkconnect, corinv);
        // re-precompile clkconnect after the update
        clkconnect = sort_clkconnect(szclkIN, clkconnect);

        howclk=size(bllst);
    elseif szclkIN == 1 then
        // Only one event input and one present block so just use block index
        howclk=clkIN;
    elseif szclkIN == [] & ~ALWAYS_ACTIVE then
        // when no events, just add one fictious event
        [bllst, corinv] = add_fictious_single(bllst, corinv);
        
        howclk=size(bllst);
    elseif szclkIN==1  then
        howclk=allhowclk;
    else
        howclk = [];
    end

    // in case of any event inputs, encode clkconnect with the fictious event input block.
    if ~(ALWAYS_ACTIVE&szclkIN==[]) then
        n=size(cap,1)
        for i=1:n
            if szclkIN>1 then
                for j=1:(2^szclkIN)-1
                    clkconnect=[clkconnect;[howclk j cap(i) 1]];
                end
            elseif szclkIN == 1 then
                clkconnect=[clkconnect;[howclk 1 cap(i) 1]];
            end
        end
        
        // Update event flags
        for i=1:2^n-1
            vec=ptccgen_utils_toBinary(i,n);
            for j=find(vec == 1)
                clkconnect=[clkconnect;[howclk i cap(j) 1]];
            end
        end
    end
endfunction

