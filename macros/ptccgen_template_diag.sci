// This file is part of the Pthread C Code generator toolbox
// 
// Copyright (C) 2011 - DIGITEO - Clément DAVID <clement.david@scilab.org>
// Copyright (C) 2011-2013 - Scilab Enterprises - Clément DAVID <clement.david@scilab-enterprises.com>
// see license.Code for more licensing informations

function [scs_m] = ptccgen_template_diag()
    // Emit an MPPA template block architecture
    //
    // Calling Sequence
    //   [scs_m] = ptccgen_template_diag();
    //
    // Return
    // scs_m: scicos_diagram: a diagram of a block
    //

    [m,p]=libraryinfo('pthread_ccodegenlib');

    scs_m = scicos_diagram();
    scs_m.props.context = ['Fechantillon=32E3;';
    'Fsin=5E3;';
    'Fscope=Fechantillon;';
    '';
    'ScopePeriod=300;';
    'bruit=0.2;';
    '';
    'N_THREAD_PER_CLUSTER=15;';
    'N_THIRD_ORDER_FILTERS=16;';
    '';
    'hz=list();';
    'for num=1:N_THREAD_PER_CLUSTER'
    '  if num==1 then';
    '    ftype=''lp'';';
    '    frq=0.5/(N_THREAD_PER_CLUSTER +1);';
    '  else';
    '    ftype=''bp'';';
    '    frq=0.5/(N_THREAD_PER_CLUSTER +1) * [num-1 num];';
    '  end';
    '  hz($+1) = iir(3, ftype, ''butt'', frq, []);';
    'end';
    ''
    ''];
    execstr(scs_m.props.context);
    scs_m.props.tf=ScopePeriod/Fscope;

    // overload the get_value to easily "set" some exprs
    %scicos_prob=%f;
    %scicos_context=struct();
    prot = funcprot();
    funcprot(0);
    scicos_getvalue=setvalue;
    getvalue=setvalue;
    funcprot(prot);

    function [thread] = create_thread(num)
        thread = SUPER_f("define");

        thread_scs_m = scicos_diagram();
        thread_scs_m.props.title = msprintf("Thread %d", num);

        // add IN
        in = IN_f("define");
        in.graphics.orig = [40 10];
        in.graphics.sz = [20 20];

        in.graphics.pout = 1+2*N_THIRD_ORDER_FILTERS+3;

        thread_scs_m.objs($+1) = in;

        // add N_THIRD_ORDER_FILTERS custom DLR
        for i=1:N_THIRD_ORDER_FILTERS
            dlr = DLR("define");
            dlr.graphics.orig = [100 0] + 120*[i-1 0];
            dlr.graphics.sz = [80 40];

            dlr.graphics.exprs = [msprintf("hz(%d).num", num);msprintf("hz(%d).den", num)];
            dlr = DLR("set", dlr);

            dlr.graphics.pin = 1+2*N_THIRD_ORDER_FILTERS+2 + i;
            dlr.graphics.pout = 1+2*N_THIRD_ORDER_FILTERS+2 + i+1;
            dlr.graphics.pein = 1+3*N_THIRD_ORDER_FILTERS+3 + 2*i;

            thread_scs_m.objs($+1) = dlr;
        end

        // add OUT
        out = OUT_f("define");
        out.graphics.orig = [100 10] + 120*[N_THIRD_ORDER_FILTERS 0];
        out.graphics.sz = [20 20];

        out.graphics.pin = 1+3*N_THIRD_ORDER_FILTERS+3;

        thread_scs_m.objs($+1) = out;

        // add CLKINV_f
        clkin = CLKINV_f("define");
        clkin.graphics.orig = [130 100];
        clkin.graphics.sz = [20 20];

        clkin.graphics.peout = 1+3*N_THIRD_ORDER_FILTERS+3 + 1;

        thread_scs_m.objs($+1) = clkin;

        // split clock
        for i=1:N_THIRD_ORDER_FILTERS
            split = CLKSPLIT_f("define");
            split.graphics.orig = [137 67] + 120*[i-1 0];
            split.graphics.sz = [7 7];

            split.graphics.pein=1+3*N_THIRD_ORDER_FILTERS+2 + 2*i;
            split.graphics.peout=[1+3*N_THIRD_ORDER_FILTERS+2 + 2*i+1 ; 1+3*N_THIRD_ORDER_FILTERS+2 + 2*i+2];

            thread_scs_m.objs($+1) = split;
        end


        l = scicos_link();
        l.id = msprintf("IN_f to DLR%d", 1);
        l.from = [1 1 0];
        l.to = [2 1 1];

        thread_scs_m.objs($+1) = l;

        for i=2:N_THIRD_ORDER_FILTERS
            l = scicos_link();
            l.id = msprintf("DLR%d to DLR%d", i-1, i);
            l.from = [i 1 0];
            l.to = [i+1 1 1];

            thread_scs_m.objs($+1) = l;
        end

        l = scicos_link();
        l.id = msprintf("DLR%d to OUT_f", N_THIRD_ORDER_FILTERS);
        l.from = [1+N_THIRD_ORDER_FILTERS 1 0];
        l.to = [1+N_THIRD_ORDER_FILTERS+1 1 1];

        thread_scs_m.objs($+1) = l;

        for i=1:N_THIRD_ORDER_FILTERS
            l = scicos_link();
            if i==1 then
                l.id = msprintf("CLKIN to CLKSPLIT%d", i);
                l.from = [N_THIRD_ORDER_FILTERS+2+i, 1, 0];
            else
                l.id = msprintf("CLKSPLIT%d to CLKSPLIT%d", i-1, i);
                l.from = [N_THIRD_ORDER_FILTERS+2+i, 2, 0];
            end
            l.ct = [5 -1];
            l.to = [N_THIRD_ORDER_FILTERS+3+i 1 1];

            thread_scs_m.objs($+1) = l;

            l = scicos_link();
            l.id = msprintf("CLKSPLIT%d to DLR%d", i, i);
            l.ct = [5 -1];
            l.from = [N_THIRD_ORDER_FILTERS+3+i 1 0];
            l.to = [i+1 1 1];

            thread_scs_m.objs($+1) = l;
        end

        thread.model.rpar = thread_scs_m;
    endfunction

    function [cluster] = create_cluster()
        // only a unique cluster is supported yet
        cluster = SUPER_f("define");

        cluster_scs_m = scicos_diagram();
        cluster_scs_m.props.title = "Cluster 1";

        x=1;
        y=1;
        // first add the process representation
        for i=1:N_THREAD_PER_CLUSTER
            x_1=x;
            x=modulo(i-1, 4)+1;
            if x_1 > x then
                y=y+1;
            end
            clear x_1

            // thread block
            thread = create_thread(i)
            thread.graphics.orig = [200 740] + [x-1 -y+1] * 160 + [0 x-1] * -40;
            thread.graphics.sz = [40 40];
            thread.graphics.id = msprintf("Thread %d", 4*(y-1)+x);

            thread.graphics.pin=3*N_THREAD_PER_CLUSTER+5+4*(4*(y-1)+x)-2;
            thread.graphics.pout=3*N_THREAD_PER_CLUSTER+5+4*(4*(y-1)+x)-1;

            thread.model.evtin=-1;
            thread.graphics.pein=3*N_THREAD_PER_CLUSTER+5+4*(4*(y-1)+x);

            cluster_scs_m.objs($+1) = thread;

            // source clock
            clkfrom = CLKFROM("define");
            clkfrom.graphics.orig = [200 810] + [x-1 -y+1] * 160 + [0 x-1] * -40;
            clkfrom.graphics.sz = [40 20];

            clkfrom.graphics.peout=3*N_THREAD_PER_CLUSTER+5+4*(4*(y-1)+x);

            cluster_scs_m.objs($+1) = clkfrom;

            // split clock
            split = SPLIT_f("define");
            split.graphics.orig = [157 757] + [0 -y+1] * 160 + [0 x-1] * -40;
            split.graphics.sz = [7 7];

            split.graphics.pin=3*N_THREAD_PER_CLUSTER+5+4*(4*(y-1)+x)-4-2;
            split.graphics.pout=3*N_THREAD_PER_CLUSTER+5+4*(4*(y-1)+x)-2;

            cluster_scs_m.objs($+1) = split;
        end

        // add a custom mux
        mux = MUX("define");
        mux.graphics.orig = [800 160]
        mux.graphics.sz = [10 40]+N_THREAD_PER_CLUSTER*[0 40];

        mux.graphics.exprs = "ones(N_THREAD_PER_CLUSTER,1)"
        mux = MUX("set", mux);

        mux.graphics.pin = 3*N_THREAD_PER_CLUSTER+5+4*(1:N_THREAD_PER_CLUSTER)-1;
        mux.graphics.pout = 3*N_THREAD_PER_CLUSTER+5+4*N_THREAD_PER_CLUSTER + 1;

        cluster_scs_m.objs($+1) = mux;

        // add IN
        in = IN_f("define");
        in.graphics.orig = [40 750];
        in.graphics.sz = [20 20];

        in.graphics.pout = 3*N_THREAD_PER_CLUSTER+5;

        cluster_scs_m.objs($+1) = in;

        // add OUT
        out = OUT_f("define");
        out.graphics.orig = [880 470];
        out.graphics.sz = [20 20];

        out.graphics.pin = 3*N_THREAD_PER_CLUSTER+5+4*N_THREAD_PER_CLUSTER + 1;

        cluster_scs_m.objs($+1) = out;

        // add CLKINV_f
        clkin = CLKINV_f("define");
        clkin.graphics.orig = [440 910];
        clkin.graphics.sz = [20 20];

        clkin.graphics.peout = 3*N_THREAD_PER_CLUSTER+5+4*N_THREAD_PER_CLUSTER + 2;

        cluster_scs_m.objs($+1) = clkin;

        // add CLKGOTO
        clkin = CLKGOTO("define");
        clkin.graphics.orig = [430 860];
        clkin.graphics.sz = [40 20];

        clkin.graphics.pein = 3*N_THREAD_PER_CLUSTER+5+4*N_THREAD_PER_CLUSTER + 2;

        cluster_scs_m.objs($+1) = clkin;

        // add the links
        for i=1:N_THREAD_PER_CLUSTER
            split_link = scicos_link();
            in_link = scicos_link();
            out_link = scicos_link();
            clk_link = scicos_link();

            if i==1 then
                split_link.id = msprintf("IN to SPLIT %d", i);
                split_link.from = [3*N_THREAD_PER_CLUSTER+2, 1, 0];
            else
                split_link.id = msprintf("SPLIT %d to SPLIT %d", i-1, i);
                split_link.from = [3*(i-1), 2, 0];
            end
            split_link.to = [3*i, 1, 1];

            in_link.id = msprintf("SPLIT to THREAD %d", i);
            in_link.from = [3*i, 1, 0];
            in_link.to = [3*i-2, 1, 1];

            out_link.id = msprintf("THREAD %d to MUX", i);
            out_link.from = [3*i-2, 1, 0];
            out_link.to = [3*N_THREAD_PER_CLUSTER+1, i, 1];

            clk_link.id = msprintf("CLKFROM %d to THREAD", i);
            clk_link.ct = [5 -1];
            clk_link.from = [3*i-1, 1, 0];
            clk_link.to = [3*i-2, 1, 0];

            cluster_scs_m.objs($+1) = split_link;
            cluster_scs_m.objs($+1) = in_link;
            cluster_scs_m.objs($+1) = out_link;
            cluster_scs_m.objs($+1) = clk_link;
        end

        mux_link = scicos_link();
        mux_link.id = "MUX to OUT_f";
        mux_link.from = [3*N_THREAD_PER_CLUSTER+1, 1, 0];
        mux_link.to = [3*N_THREAD_PER_CLUSTER+3, 1, 1];

        clkgoto_link = scicos_link();
        clkgoto_link.id = "CLKIN to CLKGOTO";
        clkgoto_link.ct = [5 -1];
        clkgoto_link.from = [3*N_THREAD_PER_CLUSTER+4, 1, 0];
        clkgoto_link.to = [3*N_THREAD_PER_CLUSTER+5, 1, 1];

        cluster_scs_m.objs($+1) = mux_link;
        cluster_scs_m.objs($+1) = clkgoto_link;

        cluster.model.rpar = cluster_scs_m;
    endfunction

    // only one cluster is supported
    cluster = create_cluster();
    cluster.graphics.orig = [240 150];
    cluster.graphics.sz = [40 40];
    cluster.graphics.id = "Cluster 1";

    cluster.graphics.pin=0;
    cluster.graphics.pout=0;

    cluster.model.evtin=-1;
    cluster.graphics.pein=0;

    scs_m.objs($+1) = cluster;

endfunction
