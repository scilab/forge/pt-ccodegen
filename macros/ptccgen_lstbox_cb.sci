// This file is part of the Pthread C Code generator toolbox
// 
// Copyright (C) 2011 - DIGITEO - Clément DAVID <clement.david@scilab.org>
// Copyright (C) 2011-2012 - Scilab Enterprises - Clément DAVID <clement.david@scilab-enterprises.com>
// see license.txt for more licensing informations

function ptccgen_lstbox_cb()
    // Set the listbox corresponding frame visible

function ptccgen_set_visible(root, status)
    // helper function against http://bugzilla.scilab.org/show_bug.cgi?id=7090
    set(root, "Visible", status);
    
    for i=1:length(root.children)
        ptccgen_set_visible(root.children(i), status);
    end
endfunction
    
    DescFrame = findobj("Tag", "ptccgen_DescFrame");
    LeftListbox = findobj("Callback", "ptccgen_lstbox_cb");
    DescTitle = findobj("Tag", "ptccgen_DescTitle");
    userData = get(DescFrame, "UserData");
    
    for data=userData
        if get(data, "Visible") == "on" then
            currentMainFrame = data;
            break;
        end
    end
    futureMainFrame = userData(LeftListbox.Value(1));

    // Update actions
    set(DescTitle, "String", gettext(get(futureMainFrame, "String")));
    if exists("currentMainFrame", "l") then
        ptccgen_set_visible(currentMainFrame, "off");
    end
    
    ptccgen_set_visible(futureMainFrame, "on");
endfunction
