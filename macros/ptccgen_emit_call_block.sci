// This file is part of the Pthread C Code generator toolbox
// 
// Copyright (C) 2011 - DIGITEO - Clément DAVID <clement.david@scilab.org>
// Copyright (C) 2011-2013 - Scilab Enterprises - Clément DAVID <clement.david@scilab-enterprises.com>
// see license.Code for more licensing informations

function [fds, state] = ptccgen_emit_call_block(fds, state, cpr, bk)
    // Emit a call to a block
    //
    // Calling Sequence
    //   [Code, state] = ptccgen_emit_call_sensor(Code, state, cpr, i);
    //
    // Parameters
    // fds: (filename, file descriptor)'s list
    // state: the current state (data stash)
    // cpr: scicos_cpr: the current cpr
    // bk: double: index of the block
    //
    // Return
    // fds: updated (filename, file descriptor)'s list
    // state: the current state (data stash)
    //

    // Get the locals from the parent scope
    nx=xptr(bk+1)-xptr(bk);
    nz=zptr(bk+1)-zptr(bk);
    nrpar=rpptr(bk+1)-rpptr(bk);
    nipar=ipptr(bk+1)-ipptr(bk);
    nin=inpptr(bk+1)-inpptr(bk);
    nout=outptr(bk+1)-outptr(bk);
    
    // ipar pointer (1 indexed)
    if nipar<>0 then ipar=ipptr(bk), else ipar=1;end
    // rpar pointer (1 indexed)
    if nrpar<>0 then rpar=rpptr(bk), else rpar=1; end
    // z pointer (0 indexed)
    if nz<>0 then z=zptr(bk)-1, else z=0;end
    // x pointer (0 indexed)
    if nx<>0 then x=xptr(bk)-1, else x=0;end


    // get the bloc name
    rdnom = state.config.out.name;

    // switch on function type
    ftyp=funtyp(bk)
    if ftyp>2000 then ftyp=ftyp-2000,end
    if ftyp>1000 then ftyp=ftyp-1000,end

    if ftyp < 0 then // ifthenelse eselect blocks
        return;
    else
        if (ftyp<>0 & ftyp<>1 & ftyp<>2 & ftyp<>3 & ftyp<>4) then
            disp("types other than 0,1,2,3 or 4 are not supported.")
            return;
        end
    end

    // emit sequencer Code
    Code = get_comment('call_blk',list(funs(bk),funtyp(bk),bk));

    // write nevprt activation
    //   nclock=abs(pt);
    Code=[Code;
    'block_'+rdnom+'['+string(bk-1)+'].nevprt=nevprt;']

    select ftyp
        // zero funtyp
    case 0 then
        //**** input/output addresses definition ****//
        if nin>1 then
            for k=1:nin
                uk=inplnk(inpptr(bk)-1+k);
                nuk=size(outtb(uk),'*');
                TYPE=mat2scs_c_ptr(outtb(uk));//scilab index start from 1
                Code=[Code;
                'rdouttb['+string(k-1)+']=('+TYPE+' *)'+rdnom+'_block_outtbptr['+string(uk-1)+'];']
            end
            Code=[Code;
            'args[0]=&(rdouttb[0]);']
        elseif nin==0
            uk=0;
            nuk=0;
            Code=[Code;
            'args[0]=(SCSREAL_COP *)'+rdnom+'_block_outtbptr[0];']
        else
            uk=inplnk(inpptr(bk));
            nuk=size(outtb(uk),'*');
            TYPE=mat2scs_c_ptr(outtb(uk));//scilab index start from 1
            Code=[Code;
            'args[0]=('+TYPE+' *)'+rdnom+'_block_outtbptr['+string(uk-1)+'];']
        end

        if nout>1 then
            for k=1:nout
                yk=outlnk(outptr(bk)-1+k);
                nyk=size(outtb(yk),'*');
                TYPE=mat2scs_c_ptr(outtb(yk));//scilab index start from 1
                Code=[Code;
                'rdouttb['+string(k+nin-1)+']=('+TYPE+' *)'+rdnom+'_block_outtbptr['+string(yk-1)+'];'];
            end
            Code=[Code;
            'args[1]=&(rdouttb['+string(nin)+']);'];
        elseif nout==0
            yk=0;
            nyk=0;
            Code=[Code;
            'args[1]=(SCSREAL_COP *)'+rdnom+'_block_outtbptr[0];'];
        else
            yk=outlnk(outptr(bk));
            nyk=size(outtb(yk),'*'),;
            TYPE=mat2scs_c_ptr(outtb(yk));//scilab index start from 1
            Code=[Code;
            'args[1]=('+TYPE+' *)'+rdnom+'_block_outtbptr['+string(yk-1)+'];'];
        end
        //*******************************************//

        //*********** call seq definition ***********//
        Codec=['(&flag,&block_'+rdnom+'['+string(bk-1)+'].nevprt,told,block_'+rdnom+'['+string(bk-1)+'].xd, \';
        'block_'+rdnom+'['+string(bk-1)+'].x,&block_'+rdnom+'['+string(bk-1)+'].nx, \';
        'block_'+rdnom+'['+string(bk-1)+'].z,&block_'+rdnom+'['+string(bk-1)+'].nz,block_'+rdnom+'['+string(bk-1)+'].evout, \';
        '&block_'+rdnom+'['+string(bk-1)+'].nevout,block_'+rdnom+'['+string(bk-1)+'].rpar,&block_'+rdnom+'['+string(bk-1)+'].nrpar, \';
        'block_'+rdnom+'['+string(bk-1)+'].ipar,&block_'+rdnom+'['+string(bk-1)+'].nipar, \';
        '(double *)args[0],&nrd_'+string(nuk)+',(double *)args[1],&nrd_'+string(nyk)+');'];
        if (funtyp(bk)>2000 & funtyp(bk)<3000)
            blank = get_blank(funs(bk)+'( ');
            Codec(1) = funs(bk)+Codec(1);
        elseif (funtyp(bk)<2000)
            Codec(1) = 'C2F('+funs(bk)+')'+Codec(1);
            blank = get_blank('C2F('+funs(bk)+') ');
        end
        Codec(2:$) = blank + Codec(2:$);
        Code = [Code;Codec];
        //*******************************************//


        //**
    case 1 then
        //**** input/output addresses definition ****//
        //       if nin>=1 then
        //         for k=1:nin
        //           uk=inplnk(inpptr(i)-1+k);
        //           nuk=size(outtb(uk),'*');
        //         end
        //       end
        //       if nout>=1 then
        //         for k=1:nout
        //           yk=outlnk(outptr(i)-1+k);
        //           nyk=size(outtb(yk),'*');
        //         end
        //       end
        //*******************************************//

        //*********** call seq definition ***********//
        Codec=['(&flag,&block_'+rdnom+'['+string(bk-1)+'].nevprt,told,block_'+rdnom+'['+string(bk-1)+'].xd, \';
        'block_'+rdnom+'['+string(bk-1)+'].x,&block_'+rdnom+'['+string(bk-1)+'].nx, \';
        'block_'+rdnom+'['+string(bk-1)+'].z,&block_'+rdnom+'['+string(bk-1)+'].nz,block_'+rdnom+'['+string(bk-1)+'].evout, \';
        '&block_'+rdnom+'['+string(bk-1)+'].nevout,block_'+rdnom+'['+string(bk-1)+'].rpar,&block_'+rdnom+'['+string(bk-1)+'].nrpar, \';
        'block_'+rdnom+'['+string(bk-1)+'].ipar,&block_'+rdnom+'['+string(bk-1)+'].nipar'];
        if (funtyp(bk)>2000 & funtyp(bk)<3000)
            blank = get_blank(funs(bk)+'( ');
            Codec(1) = funs(bk)+Codec(1);
        elseif (funtyp(bk)<2000)
            Codec(1) = 'C2F('+funs(bk)+')'+Codec(1);
            blank = get_blank('C2F('+funs(bk)+') ');
        end
        if nin>=1 | nout>=1 then
            Codec($)=Codec($)+', \'
            Codec=[Codec;'']
            if nin>=1 then
                for k=1:nin
                    uk=inplnk(inpptr(bk)-1+k);
                    nuk=size(outtb(uk),'*');
                    Codec($)=Codec($)+'(SCSREAL_COP *)'+rdnom+'_block_outtbptr['+string(uk-1)+'],&nrd_'+string(nuk)+',';
                end
                Codec($)=part(Codec($),1:length(Codec($))-1); //remove last ,
            end
            if nout>=1 then
                if nin>=1 then
                    Codec($)=Codec($)+', \'
                    Codec=[Codec;'']
                end
                for k=1:nout
                    yk=outlnk(outptr(bk)-1+k);
                    nyk=size(outtb(yk),'*');
                    Codec($)=Codec($)+'(SCSREAL_COP *)'+rdnom+'_block_outtbptr['+string(yk-1)+'],&nrd_'+string(nyk)+',';
                end
                Codec($)=part(Codec($),1:length(Codec($))-1); //remove last ,
            end
        end

        if ztyp(bk) then
            Codec($)=Codec($)+', \'
            Codec=[Codec;'w,&nrd_0);'];
        else
            Codec($)=Codec($)+');';
        end

        Codec(2:$) = blank + Codec(2:$);
        Code = [Code;Codec];
        //*******************************************//

        //**
    case 2 then

        //*********** call seq definition ***********//
        Codec=[funs(bk)+'(&flag,&block_'+rdnom+'['+string(bk-1)+'].nevprt,told,block_'+rdnom+'['+string(bk-1)+'].xd, \';
        'block_'+rdnom+'['+string(bk-1)+'].x,&block_'+rdnom+'['+string(bk-1)+'].nx, \';
        'block_'+rdnom+'['+string(bk-1)+'].z,&block_'+rdnom+'['+string(bk-1)+'].nz,block_'+rdnom+'['+string(bk-1)+'].evout, \';
        '&block_'+rdnom+'['+string(bk-1)+'].nevout,block_'+rdnom+'['+string(bk-1)+'].rpar,&block_'+rdnom+'['+string(bk-1)+'].nrpar, \';
        'block_'+rdnom+'['+string(bk-1)+'].ipar,&block_'+rdnom+'['+string(bk-1)+'].nipar, \';
        '(double **)block_'+rdnom+'['+string(bk-1)+'].inptr,block_'+rdnom+'['+string(bk-1)+'].insz,&block_'+rdnom+'['+string(bk-1)+'].nin, \';
        '(double **)block_'+rdnom+'['+string(bk-1)+'].outptr,block_'+rdnom+'['+string(bk-1)+'].outsz, &block_'+rdnom+'['+string(bk-1)+'].nout'];
        if ~ztyp(bk) then
            Codec($)=Codec($)+');';
        else
            Codec($)=Codec($)+', \';
            Codec=[Codec;
            'block_'+rdnom+'['+string(bk-1)+'].g,&block_'+rdnom+'['+string(bk-1)+'].ng);']
        end
        blank = get_blank(funs(bk)+'( ');
        Codec(2:$) = blank + Codec(2:$);
        Code = [Code;Codec];
        //*******************************************//

        //**
    case 4 then
        Code=[Code;
        funs(bk)+'(&block_'+rdnom+'['+string(bk-1)+'],flag);'];

    end

    // get the sequencer txt
    sequencer = txt("sequence");

    // set the sequencer txt
    sequencer($+1) = Code;
    txt("sequence") = sequencer;

endfunction

// get_comment : return a C comment
//               for generated code
//
//input : typ : a string
//        param : a list
//
//output : a C comment
//
function [Code]=get_comment(typ,param)
    Code = [];
    select typ
        //** main flag
    case 'flag' then
        select param(1)
        case 0 then
            Code = '/* Continuous state computation */'
        case 1 then
            Code = '/* Output computation */'
        case 2 then
            Code = '/* Discrete state computation */'
        case 3 then
            Code = '/* Output Event computation */'
        case 4 then
            Code = '/* Initialization */'
        case 5 then
            Code = '/* Ending */'
        case 9 then
            Code = '/* Update zero crossing surfaces */'
        end
        //** blocks activated on event number
    case 'ev' then
        Code = '/* Blocks activated on the event number '+string(param(1))+' */'

        //** blk calling sequence
    case 'call_blk' then
        Code = ['/* Call of '''+param(1) + ...
        ''' (type '+string(param(2))+' - blk nb '+...
        string(param(3))];
        if ztyp(param(3)) then
            Code=Code+' - with zcross) */';
        else
            Code=Code+') */';
        end
        //** proto calling sequence
    case 'proto_blk' then
        Code = ['/* prototype of '''+param(1) + ...
        ''' (type '+string(param(2))];
        if ztyp(param(3)) then
            Code=Code+' - with zcross) */';
        else
            Code=Code+') */';
        end
        //** ifthenelse calling sequence
    case 'ifthenelse_blk' then
        Code = ['/* Call of ''if-then-else'' blk (blk nb '+...
        string(param(1))+') */']
        //** eventselect calling sequence
    case 'evtselect_blk' then
        Code = ['/* Call of ''event-select'' blk (blk nb '+...
        string(param(1))+') */']
        //** set block structure
    case 'set_blk' then
        Code = ['/* set blk struc. of '''+param(1) + ...
        ''' (type '+string(param(2))+' - blk nb '+...
        string(param(3))+') */'];
        //** Update xd vector ptr
    case 'update_xd' then
        Code = ['/* Update xd vector ptr */'];
        //** Update g vector ptr
    case 'update_g' then
        Code = ['/* Update g vector ptr */'];
    else
        break;
    end
endfunction
