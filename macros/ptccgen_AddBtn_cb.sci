// This file is part of the Pthread C Code generator toolbox
// 
// Copyright (C) 2011 - DIGITEO - Clément DAVID <clement.david@scilab.org>
// Copyright (C) 2011-2012 - Scilab Enterprises - Clément DAVID <clement.david@scilab-enterprises.com>
// see license.txt for more licensing informations

function ptccgen_AddBtn_cb(tag, file_mask)
    obj = findobj("Tag", tag);
    
    if exists("file_mask", 'l') then
        directory = obj.string($);
        value = uigetfile(file_mask, directory, gettext("Select a file"), %t);
    else
        value = x_dialog("", "")';
    end
    
    strings = get(obj, "String");
    strings = cat(2, strings, value);
    set(obj, "String", strings);
endfunction
