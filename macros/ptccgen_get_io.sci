// This file is part of the Pthread C Code generator toolbox
// 
// Copyright (C) 2011 - DIGITEO - Clément DAVID <clement.david@scilab.org>
// Copyright (C) 2011-2012 - Scilab Enterprises - Clément DAVID <clement.david@scilab-enterprises.com>
// see license.txt for more licensing informations


function [in_idx, out_idx, ein_idx, eout_idx] = ptccgen_get_io(bllst)
    // Get the I/O blocks indexes in the block list.
    //
    // Calling Sequence
    //   [in_idx, out_idx, ein_idx, eout_idx] = ptccgen_get_io(bllst)
    //
    // Parameters
    // bllst: list of blocks: the pre-compiled block list
    // in_idx: vector: Input block index
    // out_idx: vector: Output block numbers
    // ein_idx: vector: Event input block numbers
    // eout_idx: vector: Event output ports numbers
    
    in_idx = [];
    out_idx = [];
    ein_idx = [];
    eout_idx = [];
    
    tt = [];

    for i=1:size(bllst)
        // check for a scilab function block
        if type(bllst(3).sim(1)) == 13 then
            error(msprintf(gettext("%s: superblock should not contains any Scilab function block."), "ptccgen_get_io"));
        end

        for j=1:size(bllst)
            if (bllst(i).sim(1)=='actionneur'+string(j)) then
                if tt<>i then
                    in_idx = [in_idx; i];
                    tt = i;
                end
            elseif (bllst(i).sim(1)=='capteur'+string(j)) then
                if tt<>i then
                    out_idx = [out_idx; i];
                    tt = i;
                end
            elseif (bllst(i).sim(1)=='bidon') then
                if tt<>i then
                    ein_idx = [ein_idx; i];
                    tt = i;
                end
            elseif (bllst(i).sim(1)=='bidon2') then
                if tt<>i then
                    eout_idx = [eout_idx; i];
                    tt = i;
                end
            end
        end
    end
endfunction

