// This file is part of the Pthread C Code generator toolbox
// 
// Copyright (C) 2011 - DIGITEO - Clément DAVID <clement.david@scilab.org>
// Copyright (C) 2011-2012 - Scilab Enterprises - Clément DAVID <clement.david@scilab-enterprises.com>
// see license.txt for more licensing informations


function ptccgen_validate_funs(sim)
    // Validate functions names and types for code generation
    //
    // Calling Sequence
    //   ptccgen_validate_funs(sim)
    //
    // Parameters
    // sim: tlist: the simulation structure
    
    funs=sim.funs;
    funtyp=sim.funtyp;
    clkptr=sim.clkptr;
    
    for i=1:size(funs)
        if funtyp(i) == 3 then
            error(msprintf(gettext("%s: scilab functions blocks not supported.\n"), "ptccgen_validate_funs"));
        end
        
// FIXME: bloc generating activation is supported
//        
//        if  (clkptr(i+1) - clkptr(i)) <> 0 &..
//            funtyp(i) > -1 &..
//            funs(i) ~= 'bidon2' then
//            
//            error(msprintf(gettext("%s: blocks generating activation not supported.\n"), "ptccgen_validate_funs"));
//        end
    end
    
endfunction
