// This file is part of the Pthread C Code generator toolbox
// 
// Copyright (C) 2011 - DIGITEO - Clément DAVID <clement.david@scilab.org>
// Copyright (C) 2011-2012 - Scilab Enterprises - Clément DAVID <clement.david@scilab-enterprises.com>
// see license.txt for more licensing informations


function my_config = ptccgen_gui(my_config, cpr, data)
    // GUI for the code generator configuration
    //
    // Calling Sequence
    //   my_config = ptccgen_gui(my_config, cpr, data)
    //
    // Parameters
    // my_config: ptccgen_my_config: global my_configuration
    // cpr: compiled data.
    // data: arbitrary data passed to the "Ok" callback
    //
    
    [lhs,rhs] = argn(0);
    if rhs <> 3 then
        error(msprintf(gettext("%s: Wrong number of input arguments: %d expected.\n"), "ptccgen_gui", 2));
    end
    
    // ---- Static parameters ----
        
    // Figure width & height
    figwidth         = 600;
    figheight        = 480;

    // Margin
    margin           = 10;
    widgetHeight     = 25;

    // Message Frame
    msgWidth         = figwidth -2*margin;
    msgHeight        = 30;

    // Button
    buttonHeight     = 20;

    // Font Size
    defaultFontSize  = 12;

    // if it already exists, recreate
    oldFig = findobj("tag", "ptccgenFigure");
    if ~isempty(oldFig) then
        delete(oldFig);
    end
    
    // ---- Gui creation ----
    
    ptccgenFigure = figure( ..
        "figure_name", gettext("Xcos POSIX Thread C Code Generator"), ..
        "position"   , [0 0 figwidth figheight],..
        "background" , -2,..
        "UserData"   , list(my_config, cpr, data), ..
        "tag"        , "ptccgenFigure");
    
    delmenu(ptccgenFigure.figure_id, gettext("&File"));
    delmenu(ptccgenFigure.figure_id, gettext("&Tools"));
    delmenu(ptccgenFigure.figure_id, gettext("&Edit"));
    delmenu(ptccgenFigure.figure_id, gettext("&?"));
    toolbar(ptccgenFigure.figure_id, "off");
    
    // force figure size after menu removing
    ptccgenFigure.axes_size = [figwidth figheight];
    
    // ---- Left pane ----
    listboxWidth              = 150;
    listboxFrameWidth         = listboxWidth + 2*margin;

    listboxFrameHeight        = figheight- 3*margin - msgHeight;
    listboxHeight             = listboxFrameHeight - 2*margin;
    
    elements = [..
        gettext("Basic settings"),..
        gettext("Compiler"),..
        gettext("Linker"),..
        ];
    
    // Frame
    LeftFrame                 = uicontrol( ..
        "Parent"              , ptccgenFigure,..
        "Style"               , "frame",..
        "Relief"              , "solid",..
        "Position"            , [margin widgetHeight+2*margin listboxFrameWidth listboxFrameHeight],..
        "Background"          , [1 1 1],..
        "Tag"                 , "LeftFrame");
    
    // Listbox
    LeftListbox               = uicontrol( ..
        "Parent"              , LeftFrame,..
        "Style"               , "listbox",..
        "Position"            , [ margin margin listboxWidth listboxHeight],..
        "Background"          , [1 1 1],..
        "FontSize"            , defaultFontSize,..
        "String"              , elements,..
        "Callback"            , "ptccgen_lstbox_cb", ..
        "Min"                 , 1, ..
        "Max"                 , 1);
    
    // ---- Main frame ----
    descFrameWidth           = figwidth - listboxFrameWidth - 3*margin;
    descFrameHeight          = listboxFrameHeight;
    
    descWidth                = descFrameWidth  - 2*margin;
    descHeight               = descFrameHeight - 4*margin;
    
    // Frame
    DescFrame                = uicontrol( ..
        "Parent"             , ptccgenFigure,..
        "Style"              , "frame",..
        "Relief"             , "solid",..
        "Background"         , [1 1 1],..
        "Position"           , [listboxFrameWidth+2*margin widgetHeight+2*margin descFrameWidth descFrameHeight],..
        "Tag"                , "ptccgen_DescFrame");
    
    // Frame title
    DescTitle                = uicontrol( ..
        "Parent"             , DescFrame,..
        "Style"              , "text",..
        "Position"           , [2*margin descFrameHeight-1.5*margin 200 widgetHeight],..
        "HorizontalAlignment", "center",..
        "VerticalAlignment"  , "middle",..
        "FontWeight"         , "bold",..
        "FontSize"           , 12,..
        "Background"         , [1 1 1],..
        "Tag"                , "ptccgen_DescTitle");
    
    position                 = [ margin margin descWidth descHeight],
    set(DescFrame, "userData", list(..
        createSettingsFrame(DescFrame, position),..
        createCompilerFrame(DescFrame, position),..
        createLinkerFrame(DescFrame, position)));
    
    set(LeftListbox, "Value", 1);
    ptccgen_lstbox_cb();
    
    // ---- OK /Cancel ----
    // Frame
    msgFrame                 = uicontrol( ..
        "Parent"             , ptccgenFigure,..
        "Style"              , "frame",..
        "Relief"             , "solid",..
        "Background"         , [1 1 1],..
        "Position"           , [margin margin msgWidth msgHeight],..
        "Tag"                , "ptccgen_msgFrame");

    // Text
    msgText                  = uicontrol( ..
        "Parent"             , msgFrame,...
        "Style"              , "text",..
        "HorizontalAlignment", "left",..
        "VerticalAlignment"  , "middle",..
        "String"             , "", ..
        "FontSize"           , 12,..
        "Background"         , [1 1 1],..
        "Position"           , [2 2 328 msgHeight-4],..
        "Tag"                , "ptccgen_msgText");

    okBtn                    = uicontrol(..
        "Parent"             , msgFrame,..
        "Style"              , "pushbutton",..
        "String"             , gettext("Ok"),..
        "Position"           , [350 margin/2 80 buttonHeight],..
        "Callback"           , "ptccgen_gui_cb(""ok"")",..
        "Tag"                , "ptccgen_okBtn");

    cancelBtn                = uicontrol(..
        "Parent"             , msgFrame,..
        "Style"              , "pushbutton",..
        "String"             , gettext("Cancel"),..
        "Position"           , [450 margin/2 80 buttonHeight],..
        "Callback"           , "ptccgen_gui_cb(""cancel"")",..
        "Tag"                , "ptccgen_cancelBtn");
endfunction

function frame = createSettingsFrame(DescFrame, position)
    userData = get(get(DescFrame, "Parent"), "UserData");

    frame = uicontrol( ..
        "Parent"             , DescFrame,..
        "Style"              , "frame",..
        "String"             , "Basic settings",..
        "Position"           , position,..
        "Background"         , [1 1 1],..
        "Visible"            , "off");
    
    blkNameLbl = uicontrol( ..
        "Parent"             , frame,..
        "Style"              , "text",..
        "String"             , gettext("Block name"),..
        "Position"           , [(position(1) + margin), (position(4)  - (buttonHeight + margin)), 120, buttonHeight],..
        "Background"         , [1 1 1]);
    
    blkNameTxt = uicontrol( ..
        "Parent"             , frame,..
        "Style"              , "edit",..
        "String"             , userData(1).out.name,..
        "Position"           , [(position(1) + margin + 120), (position(4) - (buttonHeight + margin)), 200, buttonHeight],..
        "Background"         , [1 1 1],..
        "Tag"                , "ptccgen_blkNameTxt");
    
    targetDirLbl = uicontrol( ..
        "Parent"             , frame,..
        "Style"              , "text",..
        "String"             , gettext("Target directory"),..
        "Position"           , [(position(1) + margin), (position(4) - 2*(buttonHeight + margin)), 120, buttonHeight],..
        "Background"         , [1 1 1]);
    
    targetDirTxt = uicontrol( ..
        "Parent"             , frame,..
        "Style"              , "edit",..
        "String"             , userData(1).out.name,..
        "Position"           , [(position(1) + margin + 120), (position(4) - 2*(buttonHeight + margin)), 200, buttonHeight],..
        "Background"         , [1 1 1],..
        "Tag"                , "ptccgen_targetDirTxt");

    targetDirBtn = uicontrol( ..
        "Parent"             , frame,..
        "Style"              , "pushbutton",..
        "String"             , "...",..
        "Callback"           , "targetDirBtn_cb",..
        "Position"           , [(position(1) + 2*margin + 320), (position(4) - 2*(buttonHeight + margin)), buttonHeight, buttonHeight]);
     
     standaloneLbl = uicontrol(..
        "Parent"             , frame,..
        "Style"              , "text",..
        "String"             , gettext("Standalone"),..
        "Position"           , [(position(1) + margin), (position(4) - 4*(buttonHeight + margin)), 120, buttonHeight],..
        "Background"         , [1 1 1]);

     standaloneChk = uicontrol(..
        "Parent"             , frame,..
        "Style"              , "checkbox",..
        "Value"              , userData(1).options.standalone,..
        "Position"           , [(position(1) + margin + 120), (position(4) - 4*(buttonHeight + margin)), buttonHeight, buttonHeight],..
        "Background"         , [1 1 1],..
        "Tag"                , "ptccgen_standaloneChk");
     
    updateLbl = uicontrol(..
        "Parent"             , frame,..
        "Style"              , "text",..
        "String"             , gettext("Update diagram"),..
        "Position"           , [(position(1) + margin), (position(4) - 5*(buttonHeight + margin)), 120, buttonHeight],..
        "Background"         , [1 1 1]);

     updateChk = uicontrol(..
        "Parent"             , frame,..
        "Style"              , "checkbox",..
        "Value"              , userData(1).options.updateDiagram,..
        "Position"           , [(position(1) + margin + 120), (position(4) - 5*(buttonHeight + margin)), buttonHeight, buttonHeight],..
        "Background"         , [1 1 1],..
        "Tag"                , "ptccgen_updateChk");
        
    statsLbl = uicontrol(..
        "Parent"             , frame,..
        "Style"              , "text",..
        "String"             , formatStats(userData(2)),..
        "Position"           , [(position(1) + margin), (position(2) + 2 * margin), (position(3) - 3*margin), 150]);
endfunction

function str = formatStats(cpr, data)
    [act, cap, allhowclk, allhowclk2] = data.io(:);
    str = string([]);
    str = str + "<html><body><table><tr>";
        str = str + "<td>" + gettext("Number of blocks :") + "</td>";
        str = str + msprintf("<td>%d</td>", length(cpr.sim.funs));
    str = str + "</tr><tr>";
        str = str + "<td>" + gettext("Number of activations :") + "</td>";
        str = str + msprintf("<td>%d</td>", length(cpr.state.tevts));
    str = str + "</tr><tr>";
        str = str + "<td>" + gettext("Number of input ports :") + "</td>";
        str = str + msprintf("<td>%d</td>", size(act, '*'));
    str = str + "</tr><tr>";
        str = str + "<td>" + gettext("Number of output ports :") + "</td>";
        str = str + msprintf("<td>%d</td>", size(cap, '*'));
    str = str + "</tr><tr>";
        str = str + "<td></td>";
        str = str + "<td></td>";
    str = str + "</tr><tr>";
//        str = str + "<td>" + gettext("Block with work :") + "</td>";
//        str = str + "<td>" + "FIXME" + "</td>";
    str = str + "</tr><tr>";
        str = str + "<td>" + gettext("Size of states :") + "</td>";
        str = str + "<td>" + string(length(cpr.state.z)) + "</td>";
    str = str + "</tr><tr>";
        str = str + "<td>" + gettext("Size of continuous states :") + "</td>";
        str = str + "<td>" + string(length(cpr.state.x)) + "</td>";
    str = str + "</tr></table></body></html>";
endfunction

function frame = createCompilerFrame(DescFrame, position)
    userData = get(get(DescFrame, "Parent"), "UserData");
    
    frame = uicontrol( ..
        "Parent"             , DescFrame,..
        "Style"              , "frame",..
        "String"             , "Compiler",..
        "Position"           , position,..
        "Background"         , [1 1 1],..
        "Visible"            , "off");
    
    CflagsLbl = uicontrol( ..
        "Parent"             , frame,..
        "Style"              , "text",..
        "String"             , gettext("Compiler flags"),..
        "Position"           , [(position(1) + margin), (position(4)  - (buttonHeight + margin)), 120, buttonHeight],..
        "Background"         , [1 1 1],..
        "Visible"            , "off");
    
    CflagsLst = uicontrol( ..
        "Parent"             , frame,..
        "Style"              , "listbox",..
        "String"             , userData(1).out.cflags + "",..
        "Position"           , [(position(1) + margin + 120), (position(4)  - (buttonHeight + margin) - 6*buttonHeight), 230, 7*buttonHeight],..
        "Background"         , [1 1 1],..
        "Visible"            , "off",..
        "Min"                , 0,..
        "Max"                , 2,..
        "Tag"                , "ptccgen_CflagsLst");

    CflagsAddBtn = uicontrol( ..
        "Parent"             , frame,..
        "Style"              , "pushbutton",..
        "String"             , "+",..
        "Callback"           , "ptccgen_AddBtn_cb(""ptccgen_CflagsLst"")",..
        "Position"           , [(position(1) + margin + 300), (position(4)  - 6*(buttonHeight + margin)), buttonHeight, buttonHeight],..
        "Visible"            , "off");

    CflagsDelBtn = uicontrol( ..
        "Parent"             , frame,..
        "Style"              , "pushbutton",..
        "String"             , "-",..
        "Callback"           , "ptccgen_DelBtn_cb(""ptccgen_CflagsLst"")",..
        "Position"           , [(position(1) + margin  + 300 + buttonHeight + margin), (position(4)  - 6*(buttonHeight + margin)), buttonHeight, buttonHeight],..
        "Visible"            , "off");

    CfilesLbl = uicontrol( ..
        "Parent"             , frame,..
        "Style"              , "text",..
        "String"             , gettext("Specific C files"),..
        "Position"           , [(position(1) + margin), (position(4)  - 7*(buttonHeight + margin)), 120, buttonHeight],..
        "Background"         , [1 1 1],..
        "Visible"            , "off");
    
    CfilesLst = uicontrol( ..
        "Parent"             , frame,..
        "Style"              , "listbox",..
        "String"             , userData(1).out.cfiles + "",..
        "Position"           , [(position(1) + margin + 120), (position(4)  - 7*(buttonHeight + margin) - 6*buttonHeight), 230, 7*buttonHeight],..
        "Background"         , [1 1 1],..
        "Visible"            , "off",..
        "Min"                , 0,..
        "Max"                , 2,..
        "Tag"                , "ptccgen_CfilesLst");

    file_mask = sci2exp(sci2exp("*.c"));
    CfilesAddBtn = uicontrol( ..
        "Parent"             , frame,..
        "Style"              , "pushbutton",..
        "String"             , "+",..
        "Callback"           , "ptccgen_AddBtn_cb(""ptccgen_CfilesLst"", " + file_mask + ")",..
        "Position"           , [(position(1) + margin + 300), (position(4)  - 12*(buttonHeight + margin)), buttonHeight, buttonHeight],..
        "Visible"            , "off");

    CfilesDelBtn = uicontrol( ..
        "Parent"             , frame,..
        "Style"              , "pushbutton",..
        "String"             , "-",..
        "Callback"           , "ptccgen_DelBtn_cb(""ptccgen_CfilesLst"")",..
        "Position"           , [(position(1) + margin  + 300 + buttonHeight + margin), (position(4)  - 12*(buttonHeight + margin)), buttonHeight, buttonHeight],..
        "Visible"            , "off");
    
endfunction

function frame = createLinkerFrame(DescFrame, position)
    userData = get(get(DescFrame, "Parent"), "UserData");
    
    frame = uicontrol( ..
        "Parent"             , DescFrame,..
        "Style"              , "frame",..
        "String"             , "Linker",..
        "Position"           , position,..
        "Background"         , [1 1 1],..
        "Visible"            , "off");
    
    LflagsLbl = uicontrol( ..
        "Parent"             , frame,..
        "Style"              , "text",..
        "String"             , gettext("Linker flags"),..
        "Position"           , [(position(1) + margin), (position(4)  - (buttonHeight + margin)), 120, buttonHeight],..
        "Background"         , [1 1 1],..
        "Visible"            , "off");
    
    LflagsLst = uicontrol( ..
        "Parent"             , frame,..
        "Style"              , "listbox",..
        "String"             , userData(1).out.ldflags + "",..
        "Position"           , [(position(1) + margin + 120), (position(4)  - (buttonHeight + margin) - 6*buttonHeight), 230, 7*buttonHeight],..
        "Background"         , [1 1 1],..
        "Visible"            , "off",..
        "Min"                , 0,..
        "Max"                , 1,..
        "Tag"                , "ptccgen_LflagsLst");
    
    LflagsAddBtn = uicontrol( ..
        "Parent"             , frame,..
        "Style"              , "pushbutton",..
        "String"             , "+",..
        "Callback"           , "ptccgen_AddBtn_cb(""ptccgen_LflagsLst"")",..
        "Position"           , [(position(1) + margin + 300), (position(4)  - 6*(buttonHeight + margin)), buttonHeight, buttonHeight],..
        "Visible"            , "off");

    LflagsDelBtn = uicontrol( ..
        "Parent"             , frame,..
        "Style"              , "pushbutton",..
        "String"             , "-",..
        "Callback"           , "ptccgen_DelBtn_cb(""ptccgen_LflagsLst"")",..
        "Position"           , [(position(1) + margin  + 300 + buttonHeight + margin), (position(4)  - 6*(buttonHeight + margin)), buttonHeight, buttonHeight],..
        "Visible"            , "off");
    
    LfilesLbl = uicontrol( ..
        "Parent"             , frame,..
        "Style"              , "text",..
        "String"             , gettext("Specific libraries"),..
        "Position"           , [(position(1) + margin), (position(4)  - 7*(buttonHeight + margin)), 120, buttonHeight],..
        "Background"         , [1 1 1],..
        "Visible"            , "off");
    
    LfilesLst = uicontrol( ..
        "Parent"             , frame,..
        "Style"              , "listbox",..
        "String"             , userData(1).out.lfiles + "",..
        "Position"           , [(position(1) + margin + 120), (position(4)  - 7*(buttonHeight + margin) - 6*buttonHeight), 230, 7*buttonHeight],..
        "Background"         , [1 1 1],..
        "Visible"            , "off",..
        "Min"                , 1,..
        "Max"                , 1,..
        "Tag"                , "ptccgen_LfilesLst");
    
    file_mask = sci2exp(sci2exp(["*.so";"*.dll"]));
    LfilesAddBtn = uicontrol( ..
        "Parent"             , frame,..
        "Style"              , "pushbutton",..
        "String"             , "+",..
        "Callback"           , "ptccgen_AddBtn_cb(""ptccgen_LfilesLst"", " + file_mask + ")",..
        "Position"           , [(position(1) + margin + 300), (position(4)  - 12*(buttonHeight + margin)), buttonHeight, buttonHeight],..
        "Visible"            , "off");

    LfilesDelBtn = uicontrol( ..
        "Parent"             , frame,..
        "Style"              , "pushbutton",..
        "String"             , "-",..
        "Callback"           , "ptccgen_DelBtn_cb(""ptccgen_LfilesLst"")",..
        "Position"           , [(position(1) + margin  + 300 + buttonHeight + margin), (position(4)  - 12*(buttonHeight + margin)), buttonHeight, buttonHeight],..
        "Visible"            , "off");
    
endfunction
