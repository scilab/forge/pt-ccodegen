// This file is part of the Pthread C Code generator toolbox
// 
// Copyright (C) 2011 - DIGITEO - Clément DAVID <clement.david@scilab.org>
// Copyright (C) 2011-2012 - Scilab Enterprises - Clément DAVID <clement.david@scilab-enterprises.com>
// see license.txt for more licensing informations

function ptccgen_DelBtn_cb(tag)
    obj = findobj("Tag", tag);
    strings = get(obj, "String");
    len = size(strings, '*');
    
    values = obj.value;
    if values == [] | values == 0 then
        values = len;
    end
    
    strings(values) = [];
    
    set(obj, "String", strings + "");
endfunction
