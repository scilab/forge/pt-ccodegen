// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================

valid_identifier = "valid_identifier";
if valid_identifier <> ptccgen_utils_toCName(valid_identifier) ..
    then pause, end

valid_identifier = "v123";
if valid_identifier <> ptccgen_utils_toCName(valid_identifier) ..
    then pause, end

valid_identifier = "_valid";
if valid_identifier <> ptccgen_utils_toCName(valid_identifier) ..
    then pause, end

valid_identifier = "with_space";
invalid_identifier = "with space";
if valid_identifier <> ptccgen_utils_toCName(invalid_identifier) ..
    then pause, end

valid_identifier = "withfirstnumber";
invalid_identifier = "2withfirstnumber";
if valid_identifier <> ptccgen_utils_toCName(invalid_identifier) ..
    then pause, end

valid_identifier = "with2numbers";
invalid_identifier = "2with2numbers";
if valid_identifier <> ptccgen_utils_toCName(invalid_identifier) ..
    then pause, end

// bug 4276 should be corrected
// see http://bugzilla.scilab.org/show_bug.cgi?id=4276
// valid_identifier = "with_1_numbers";
// invalid_identifier = "with 1 numbers"
// if valid_identifier <> ptccgen_utils_toCName(invalid_identifier) ..
//     then pause, end

// bug 4276 should be corrected
// see http://bugzilla.scilab.org/show_bug.cgi?id=4276
// valid_identifier = "with_invalid";
// invalid_identifier = "with #invalid"
// if valid_identifier <> ptccgen_utils_toCName(invalid_identifier) then pause, end

