// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================

len = 1;
v = 0;
if or(ptccgen_utils_toBinary(v, len) <> [0]) then pause, end

len = 32;
v = 0;
if or(ptccgen_utils_toBinary(v, len) <> zeros(1, len)) then pause, end

len = 1;
v = 1;
if or(ptccgen_utils_toBinary(v, len) <> [1]) then pause, end

len = 32;
v = 2^32 - 1;
if or(ptccgen_utils_toBinary(v, len) <> ones(1, len)) then pause, end

// length error checking
len = 32;
v = 2^32;
err = execstr("ptccgen_utils_toBinary(v, len)", "errcatch");
if err == 0 then pause, end

