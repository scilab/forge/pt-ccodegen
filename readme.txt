readme.txt of the pthread_ccodegen toolbox

This toolbox include a new code generator for an Xcos diagram.

DESCRIPTION
===========

see DESCRIPTION

AUTHORS
=======

* Clément DAVID <clement.david@scilab.org>

THANKS TO
=========

* Christophe LECLUSE <christophe.lecluse@kalray.eu>
* Pierre-Yves THOULON <pierre-yves.thoulon@kalray.eu>

